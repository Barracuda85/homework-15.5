﻿// HomeWork 15.4.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <ostream>
#include <iostream>
using namespace std;

//1. В главном исполняемом файле (файл, в котором находится функция main), используя цикл и условия, 
//   выведите в консоль все чётные числа от 0 до N (N задайте константой в начале программы).
//2. Напишите функцию, которая в зависимости от своих параметров печатает в консоль или чётные, 
//   или нечётные числа от 0 до N(N тоже сделайте параметром функции).
//3. Минимизируйте количество циклов и условий.



int main()
{
	setlocale(LC_ALL, "Rus");

	int i = 0;
	int j = 25;
	cout << "While Test\n";//Тест цикла через While
	while (i<j)
	{
		i++;
		cout << i << " ";
	}

	cout << endl;



	cout << "For Test\n";//Тест Цикла через For
	cout << "Normal numbers\n";//Простые числа

	for (i = 0; i < j; i=i++)
	{
		cout << i << " ";
	}

	cout << endl;
	cout << "Odd numbers\n";//Четные числа

	for (i = 0; i < j; i++)
	{
		if (i % 2 == 0)
		{
			cout << i << " ";
		}		
	}

	cout << endl;
	cout << "Even numbers\n";//Не четные числа

	for (i = 0; i < j; i++)
	{
		if (i % 2 != 0)
		{
			cout << i << " ";
		}
	}
	cout << endl;

}

